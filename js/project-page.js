const brandingDesign = document.getElementById('branding-design');
const clippingPath = document.getElementById('clipping-path');
const adsDesign = document.getElementById('ads-design');
const apparelDesign = document.getElementById('apparel-design');
const illustration = document.getElementById('illustration');
const photoRetouch = document.getElementById('photo-retouch');
const uiUxDesign = document.getElementById('ui-ux-design');
const stickerDesign = document.getElementById('sticker-design');
const logoDesign = document.getElementById('logo-design');
const packagingDesign = document.getElementById('packaging-design');
const printDesign = document.getElementById('print-design');
const othersDesign = document.getElementById('others-design');


brandingDesign.setAttribute('src','img/project-page/branding.svg');
clippingPath.setAttribute('src','img/project-page/clipping path.svg');
adsDesign.setAttribute('src','img/project-page/adas.svg');
apparelDesign.setAttribute('src','img/project-page/apparel.svg');
illustration.setAttribute('src','img/project-page/illustration.svg');
photoRetouch.setAttribute('src','img/project-page/retouch.svg');
uiUxDesign.setAttribute('src','img/project-page/ui ux.svg');
stickerDesign.setAttribute('src','img/project-page/sticker.svg');
logoDesign.setAttribute('src','img/project-page/logo.svg');
packagingDesign.setAttribute('src','img/project-page/packaging.svg');
printDesign.setAttribute('src','img/project-page/print media.svg');
othersDesign.setAttribute('src','img/project-page/other.svg');

const func = function(elementName, eventName, callback){
    elementName.addEventListener(eventName , callback)
}



func(brandingDesign,'mouseover', function(event){
    event.target.src = 'img/project-page/Mouse Over/SVG/branding.svg';

});

func(brandingDesign,'mouseleave', function(event){
    event.target.src = 'img/project-page/branding.svg';
});


func(clippingPath,'mouseover', function(event){
    event.target.src = 'img/project-page/Mouse Over/SVG/clipping path.svg';
});

func(clippingPath,'mouseleave', function(event){
    event.target.src = 'img/project-page/clipping path.svg';
});



func(adsDesign,'mouseover', function(event){
    event.target.src = 'img/project-page/Mouse Over/SVG/adas.svg';
});

func(adsDesign,'mouseleave', function(event){
    event.target.src = 'img/project-page/adas.svg';
});



func(apparelDesign,'mouseover', function(event){
    event.target.src = 'img/project-page/Mouse Over/SVG/apparel.svg';
});

func(apparelDesign,'mouseleave', function(event){
    event.target.src = 'img/project-page/apparel.svg';
});



func(illustration,'mouseover', function(event){
    event.target.src = 'img/project-page/Mouse Over/SVG/illustration.svg';
});

func(illustration,'mouseleave', function(event){
    event.target.src = 'img/project-page/illustration.svg';
});



func(photoRetouch,'mouseover', function(event){
    event.target.src = 'img/project-page/Mouse Over/SVG/retouch.svg';
});

func(photoRetouch,'mouseleave', function(event){
    event.target.src = 'img/project-page/retouch.svg';
});


func(uiUxDesign,'mouseover', function(event){
    event.target.src = 'img/project-page/Mouse Over/SVG/ui ux.svg';
});

func(uiUxDesign,'mouseleave', function(event){
    event.target.src = 'img/project-page/ui ux.svg';
});


func(stickerDesign,'mouseover', function(event){
    event.target.src = 'img/project-page/Mouse Over/SVG/sticker.svg';
});

func(stickerDesign,'mouseleave', function(event){
    event.target.src = 'img/project-page/sticker.svg';
});


func(logoDesign,'mouseover', function(event){
    event.target.src = 'img/project-page/Mouse Over/SVG/logo.svg';
});

func(logoDesign,'mouseleave', function(event){
    event.target.src = 'img/project-page/logo.svg';
});



func(packagingDesign,'mouseover', function(event){
    event.target.src = 'img/project-page/Mouse Over/SVG/packaging.svg';
});

func(packagingDesign,'mouseleave', function(event){
    event.target.src = 'img/project-page/packaging.svg';
});



func(printDesign,'mouseover', function(event){
    event.target.src = 'img/project-page/Mouse Over/SVG/print media.svg';
});

func(printDesign,'mouseleave', function(event){
    event.target.src = 'img/project-page/print media.svg';
});



func(othersDesign,'mouseover', function(event){
    event.target.src = 'img/project-page/Mouse Over/SVG/other.svg';
});

func(othersDesign,'mouseleave', function(event){
    event.target.src = 'img/project-page/other.svg';
});
