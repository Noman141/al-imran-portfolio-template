const brandingImage = document.getElementById('branding-image');
const apparelImage = document.getElementById('apparel-image');
const printImage = document.getElementById('print-design');
const packagingImage = document.getElementById('packaging-image');
const retouchImage = document.getElementById('retouch-image');
const logoImage = document.getElementById('logo-design');
const adsImage = document.getElementById('ads-image');
const uiUxImage = document.getElementById('ui/ux-image');


brandingImage.setAttribute('src','img/icons/categories/Branding.svg');
apparelImage.setAttribute('src','img/icons/categories/Apparel.svg');
printImage.setAttribute('src','img/icons//categories/Print Design.svg');
packagingImage.setAttribute('src','img/icons/categories/Packaging.svg');
retouchImage.setAttribute('src','img/icons/categories/Retouch.svg');
logoImage.setAttribute('src','img/icons/categories/logo design.svg');
adsImage.setAttribute('src','img/icons/categories/ads.svg');
uiUxImage.setAttribute('src','img/icons/categories/ui.svg');


const func = function(elementName, eventName, callback){
    elementName.addEventListener(eventName , callback)
}


func(brandingImage,'mouseover', function(event){
    event.target.src = 'img/icons/categories/Mouse Over/SVG/Branding.svg';
});

func(brandingImage,'mouseleave', function(event){
    event.target.src = 'img/icons/categories/Branding.svg';
});


func(apparelImage,'mouseover', function(event){
    event.target.src = 'img/icons/categories/Mouse Over/SVG/Apparel.svg';
});

func(apparelImage,'mouseleave', function(event){
    event.target.src = 'img/icons/categories/Apparel.svg';
});


func(printImage,'mouseover', function(event){
    event.target.src = 'img/icons/categories/Mouse Over/SVG/Print Design.svg';
});

func(printImage,'mouseleave', function(event){
    event.target.src = 'img/icons/categories/Print Design.svg';
});


func(packagingImage,'mouseover', function(event){
    event.target.src = 'img/icons/categories/Mouse Over/SVG/Packaging.svg';
});

func(packagingImage,'mouseleave', function(event){
    event.target.src = 'img/icons/categories/Packaging.svg';
});

func(retouchImage,'mouseover', function(event){
    event.target.src = 'img/icons/categories/Mouse Over/SVG/Retouch.svg';
});

func(retouchImage,'mouseleave', function(event){
    event.target.src = 'img/icons/categories/Retouch.svg';
});


func(logoImage,'mouseover', function(event){
    event.target.src = 'img/icons/categories/Mouse Over/SVG/logo design.svg';
});

func(logoImage,'mouseleave', function(event){
    event.target.src = 'img/icons/categories/logo design.svg';
});

func(adsImage,'mouseover', function(event){
    event.target.src = 'img/icons/categories/Mouse Over/SVG/ads.svg';
});

func(adsImage,'mouseleave', function(event){
    event.target.src = 'img/icons/categories/ads.svg';
});

func(uiUxImage,'mouseover', function(event){
    event.target.src = 'img/icons/categories/Mouse Over/SVG/ui.svg';
});

func(uiUxImage,'mouseleave', function(event){
    event.target.src = 'img/icons/categories/ui.svg';
});